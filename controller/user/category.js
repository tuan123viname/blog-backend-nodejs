const categoryModel = require("../../model/category.model")
const { ChangeToSlug } = require("../../helper")

function createCategory(name, image){
    return new Promise((resolve, reject) => {
        const category = new categoryModel({
            name ,
            slug : ChangeToSlug(name),
            image 
        })
        category.save()
        .then(data => {
            resolve(data);
        })
        .catch(err => {
            reject(err);
        })
    })
}

function getAllCategory(){
    return new Promise((resolve, reject) => {
        categoryModel.find()
        .then(data => {
            if(!data){
                reject(data);
            }
            resolve(data);
        })
        .catch(err => {
            reject(err);
        })
    })
}

function getCategoryById(id){
    return new Promise((resolve, reject) => {
        categoryModel.findById(id)
        .then(data => {
            if(!data){
                reject({message : 'not found'});
            }
            resolve(data);
        })
        .catch(err => {
            reject(err);
        })
    })
}

function updateCategory( data){
    return new Promise((resolve, reject) => {
        categoryModel.findByIdAndUpdate(data._id, {
            ...data,
            slug : ChangeToSlug(data.name)
        }, { upsert : true , useFindAndModify : false, new : true})
        .then(updated => {
            resolve(updated);
        })
        .catch(err => {
            reject(err);
        })
    })
}

function deleteCategory(id){
    return new Promise((resolve, reject) => {
        categoryModel.findByIdAndDelete(id)
        .then((success) => {
            if(success)
            {
                resolve({ message : 'xoa thanh cong'});
            }
            else{
                reject({ message : 'xoa that bai'})
            }
        })
        .catch(err => {
            reject({ message : 'xoa that bai'});
        })
    })
}

module.exports = {
    createCategory,
    getAllCategory,
    getCategoryById,
    updateCategory,
    deleteCategory
}