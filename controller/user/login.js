const userModel = require('../../model/user.model');
const jwt = require('../../until/jwt');
const bcryptjs = require('bcryptjs');

function login(userBody){
    return new Promise((resolve,reject) => {
        userModel.findOne({ email : userBody.email })
        .then(founded => {
            if(!founded){
                return reject({ message : 'Email khong ton tai'});
            }
            bcryptjs.compare(userBody.password, founded.password,(err, success) => {
                if(err){
                    console.log(err);
                    reject({ message : 'Mật khẩu không chính xác'});
                }
                jwt.sign({ email : userBody.email}, (err, token) => {
                    if(err)
                    {
                        reject({ message : err });
                    }
                    else{
                        const user = founded;
                        user.token = token;
                        resolve(user);
                    }
                })
            })
        })
        .catch(err => {
            reject({ message : err });
        })
    })
}

module.exports = { 
    login: login
}