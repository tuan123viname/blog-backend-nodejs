const userModel = require( '../../model/user.model');
const fs = require('fs');
const UserModal = require('../../model/user.model');
const bcryptjs = require('bcryptjs');
const jwt = require('../../until/jwt');

 function createUser(userBody){
    return new Promise((resolve, reject) => {
        userModel.findOne({ email : userBody.email })
        .then(founded => {
            if(founded){
                return reject({ message : 'Email đã tồn tại'});
            }
            bcryptjs.hash(userBody.password,10)
            .then(hash => {
                const user = new UserModal({
                    email : userBody.email,
                    password : hash
                });
                user.save()
                .then( ()=> {
                    resolve({ message : 'Đăng ký tài khoản thành công'});
                })
                .catch(err => {
                    console.log(err);
                    reject({ message : 'Lỗi server' });
                })
            })
        })
    })
}

function getUserById(id){
    return new Promise((resolve, reject) => {
        userModel.findById(id)
        .then(founded => {
            if(!founded){
                reject({message : 'không tìm thấy user'});
            }
            resolve(founded);
        })
        .catch(err => {
            reject({ message : err });
        })
    })
}

function getUserByEmail(email){
    return new Promise((resolve, reject) => {
        userModel.findOne({ email : email })
        .then(founded => {
            if(!founded){
                reject({message : 'không tìm thấy user'});
            }
            resolve(founded);
        })
        .catch(err => {
            reject({ message : err });
        })
    })
}

function getUserByToken(token){
    return new Promise((resolve, reject) => {
        jwt.verify(token, (err, data) => {
            if(err){
                console.log(err);
                reject({ message : 'token khong hop le'});
            }
            else{
                userModel.findOne({ email : data.email })
                .then(founded => {
                    if(!founded){
                        reject({ message : 'user khong ton tai'});
                    }
                    resolve( founded );
                })
                .catch(err => {
                    reject({ message : err });
                })
            }
        })
    })
}

function changeAvatar(id, image){
    return new Promise((resolve, reject) => {
        userModel.findByIdAndUpdate(id,{avatar : image }, {upsert : true })
        .then(data => {
            resolve(data);
        })
        .catch(err => {
            reject(err);
        })
    })
}

function updateUser(id,userBody){
    return new Promise((resolve, reject) => {
        const user = {
            name : userBody.name,
            address : userBody.address,
            phone : userBody.phone,
            gender : userBody.gender,
            description :userBody.description
        }
        userModel.findByIdAndUpdate(id,user,{ upsert : true , useFindAndModify : true })
        .then(data => {
            console.log(data);
            resolve(data);
        })
        .catch(err => {
            reject(err);
        })
    })
}


module.exports = {
    createUser  : createUser,
    getUserById : getUserById,
    getUserByToken : getUserByToken,
    changeAvatar   : changeAvatar,
    getUserByEmail : getUserByEmail,
    updateUser : updateUser

}