const express=require('express');
const app=express();
const bodyParser=require('body-parser');
const mongoose=require('mongoose');
const cors=require('cors');
const indexRouter=require('./router/index.router');
const errorHandle=require('./middleware/error-handle');
app.use(bodyParser.json());
app.use(cors());
app.use(bodyParser.urlencoded({extended:true}));
app.use(express.static('public'));
mongoose.connect('mongodb://localhost:27017/blog',(err)=>{
    if(err)
    {
        console.log('can not connect to mongodb');
    }
    else
    {
        console.log('successful connected to mongodb');
    }
})

app.use('/',indexRouter);
app.use(errorHandle);
app.listen('9000',()=>{
    console.log('hello ');
})
