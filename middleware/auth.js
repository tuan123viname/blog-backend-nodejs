const jwt = require("../until/jwt");
const userController = require('../controller/user/user');
function authUser(req,res,next){
    const token = req.headers['access-token'];
    if(!token){
        return res.status(401).json({ message : 'bạn không có quyền truy cập vào trang này'});
    }
    jwt.verify(token, (err, decode) => {
        if(err){
            return res.status(401).json({ message : 'token khong hop le'});
        }
        userController.getUserByEmail(decode.email)
        .then(user => {
            if(user._id)
            {
                req.user = user;
                next();
            }
            else{
                res.status(401).json({ message : 'user khong ton tai'});
            }
        })
    })
}

module.exports ={
    authUser : authUser
}