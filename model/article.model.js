const mongoose = require('mongoose');
const articleSchema = new mongoose.Schema({
    user : {
        type : mongoose.Schema.Types.ObjectId,
        ref : 'User'
    },
    slug : {
        type : String,
        required : true
    },
    title : {
        type : String,
        required : true
    },
    summary : {
        type : String,
        required : true
    },
    content : {
        type : String,
        required : true
    },
    created_at : {
        type : Date,
        default : new Date()
    },
    thumbnail : {
        type : String,
        default : ''
    },
   view : {
       type : Number,
       default : 0
   },
   category : {
    type : mongoose.Schema.Types.ObjectId,
    ref : 'Category'
   }
})

module.exports = mongoose.model('Article', articleSchema);