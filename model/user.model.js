const mongoose = require('mongoose');

const Schema = require('mongoose').Schema;

const userSchema = new Schema({
    name : {
        type: String, 
        default : 'unknown'
    },
    email : {
        type : String, 
        required : true
    },
    description: {
        type : String,
         default : ''
    },
    password : {
        type : String, 
        required: true
    },
    isActive : {
        type : Boolean,
        default : true
    },
    role : {
        type : Number,
        default : 0
    },
    avatar : {
        type : String,
        default : ''
    },
    title : {
        type : Number,
        default : 0
    },
    point : {
        type : Number,
        default : 0
    },
    token : {
        type : String,
        default : ''
    },
    gender : {
        type : String,
        default : 'unknown'
    },
    address : {
        type : String, 
        default : ''
    },
    phone: {
        type :Number,
        default : 0
    }
})

module.exports = mongoose.model('User', userSchema);