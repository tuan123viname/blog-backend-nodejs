const Router=require('express').Router();

Router.use('/user', require('./user/user.router'));
Router.use('/auth', require('./user/login.router'));
Router.use('/category', require('./user/category.router'));
module.exports=Router;
