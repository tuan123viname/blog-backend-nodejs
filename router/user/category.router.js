const categoryController = require('../../controller/user/category');
const Router = require('express').Router();
const multer = require('multer');
const { json } = require('body-parser');

var storage = multer.diskStorage({
    destination: function(req, file, cb) {
      cb(null, 'public/upload/category_image' );
    },
    filename: function(req, file, cb) {
      const uniqueSuffix = Date.now() + "-" + Math.round(Math.random() * 1e9);
      cb(null, uniqueSuffix + "-" + file.originalname);
    }
  });
  

const upload = multer({storage : storage });

Router.get('/get-all', (req,res) => {
    categoryController.getAllCategory()
    .then(data => {
        res.status(200).json(data);
    })
    .catch(err => {
        res.status(404).json(err);
    })
})

Router.get('/get-by-id/:id', (req,res) => {
    categoryController.getCategoryById(req.params.id)
    .then(data => {
        res.status(200).json(data);
    })
    .catch(err => {
        res.status(404).json(err);
    })
})

Router.post('/create',upload.single('image'), (req, res) => {
    if(!req.file){
        res.status(200).json({ message : 'fail'});
    }
    categoryController.createCategory(req.body.name,req.file.filename)
    .then(data => {
        res.status(200).json(data);
    })
    .catch(err => {
        res.status(500).json(err);
    })
})

Router.put('/update', upload.single('image') , (req, res) => {
    if(req.file)
    {
        req.body.image = req.file.filename;
    }
    categoryController.updateCategory(req.body)
    .then(data => {
        res.status(200).json(data);
    })
    .catch(err => {
        res.status(400).json(err);
    })
})

Router.delete('/delete/:id', (req, res) => {
    categoryController.deleteCategory(req.params.id)
    .then(data => {
        res.status(200).json(data);
    })
    .catch(err => {
        res.status(400).json(err);
    })
})

module.exports = Router;