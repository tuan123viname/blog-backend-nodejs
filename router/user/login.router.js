const loginController = require('../../controller/user/login');
const Router = require('express').Router();

Router.post('/login', (req, res) => {
    loginController.login(req.body)
    .then(data => {
        res.status(200).json(data);
    })
    .catch(err => {
        res.status(401).json(err);
    })
})

module.exports = Router;