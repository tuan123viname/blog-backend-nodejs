const Router = require('express').Router();
const userController = require('../../controller/user/user');
const multer = require('multer');
const { authUser } = require('../../middleware/auth');


var storage = multer.diskStorage({
    destination: function(req, file, cb) {
      cb(null, 'public/upload/user_image' );
    },
    filename: function(req, file, cb) {
      const uniqueSuffix = Date.now() + "-" + Math.round(Math.random() * 1e9);
      cb(null, uniqueSuffix + "-" + file.originalname);
    }
  });
  

const upload = multer({storage : storage });

Router.get('/get-user-by-id/:id', (req, res) => {
    userController.getUserById(req.params.id)
    .then(data => {
        res.status(200).json(data);
    })
    .catch(err => {
        res.status(404).json(err);
    })
})

Router.get('/get-user-by-token', (req, res) => {
    userController.getUserByToken(req.headers['access-token'])
    .then(data => {
        res.status(200).json(data);
    })
    .catch(err => {
        res.status(404).json(err);
    })
})

Router.post('/create', (req, res) => {
    userController.createUser(req.body)
    .then(data => {
        res.status(200).json(data);
    })
    .catch(err => {
        res.status(500).json(err);
    })
})

Router.put('/change-avatar',authUser, upload.single('avatar') ,(req,res) => {
    try {
        let image;
        if (req.file == undefined) {
          return res.status(200).send({ status: "fail" });
        } else {
          image = req.file.filename;
        }
        userController.changeAvatar(req.user._id, image).then(result => {
          return res.status(200).send(result);
        });
      } catch (err) {
        return res.status(500).send(err);
      }
    
})

Router.put('/update', authUser,(req, res) => {
    userController.updateUser(req.user._id,req.body)
    .then(data => {
        res.status(200).send(data);
    })
    .catch(err => {
        res.status(401).json(err);
    })
})




module.exports = Router;